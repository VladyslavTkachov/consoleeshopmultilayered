﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class UserDTO
    {
        public List<int> OrdersID = new List<int>();

        public Bll.DTO.Status status;
        public string Name
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }
    }
}
