﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.DTO
{
    public class OrderDTO
    {

        public List<int> products = new List<int>();
        public string Status
        {
            get;
            set;
        }

        public int Id { get; set; }
        public int IdUser { get; set; }
    }
}
