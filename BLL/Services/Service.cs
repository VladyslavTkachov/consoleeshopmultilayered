﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using DAL.Repositories;
using Database;

namespace BLL.Services
{
    public static class Performers
    {
        public static User currentUser = new User();
        public static void AddOrderToUser(int ID)
        {
            currentUser.OrdersID.Add(ID);
        }

        public static IEnumerable<Product> GetAllProduct()
        {
            ProductRepository productRepository = new ProductRepository();
            return productRepository.GetAll();
        }
        public static IEnumerable<Order> GetAllOrder()
        {
            OrderRepository orderRepository = new OrderRepository();
            return orderRepository.GetAll();
        }

        public static IEnumerable<User> GetAllUser()
        {
            UserRepository userRepository = new UserRepository();
            return userRepository.GetAll();
        }
        public static void AddProductToOrder(List<int> id, int OrderID)
        {

            ProductRepository productRepository = new ProductRepository();
            OrderRepository orderRepository = new OrderRepository();
            foreach (var item in productRepository.GetAll())
            {
                foreach (var item2 in id)
                { 
                    if (item2 == item.Id)
                    {
                        var i = productRepository.Get(item2);
                        var a = orderRepository.Get(OrderID);
                        a.products.Add(i);
                    }
                }
            }

        }

        public static void AddProductToBase(int id, string name, string category, string description, double price)
        {
            ProductRepository productRepository = new ProductRepository();
            Product product = new Product(id, name, category, description, price);
            productRepository.Create(product);
            Console.WriteLine("Успешно добавлен новый товар. Нажмите любую клавишу");
            Console.ReadKey();
        }

        public static void CreateNewUser(string name, string password)
        {
            Random rnd = new Random();
            int a = rnd.Next(1, 100);
            User user = new User(name, password, Status.User, a);
            UserRepository userRepository = new UserRepository();
<<<<<<< HEAD
=======
            userRepository.Create(user);
>>>>>>> c9dced2ac0787fd243834f71a5ebdb9b37a2c497
            foreach (var item in userRepository.GetAll())
            {
                if (item.Name == user.Name && item.Password == user.Password)
                    throw new ValidationException("Такой пользователь уже существует ", "");
            }
<<<<<<< HEAD
            userRepository.Create(user);
=======
>>>>>>> c9dced2ac0787fd243834f71a5ebdb9b37a2c497
            Console.WriteLine("Пользователь успешно создан");
            Console.WriteLine("Нажмите любую клавишу и авторизируйтесь");
            Console.ReadKey();
        }

        public static void SearchProduct(string name)
        {
            ProductRepository productRepository = new ProductRepository();
            foreach (var item in productRepository.GetAll())
            {
                if (item.Name == name)
                {
                    Console.WriteLine("Товар с таким именем найден");
                    Console.WriteLine($"{item.Id,-5} |{item.Name,-20} |{item.Category,-20} |{item.Description,-50} |{item.Price,-5}");
                    Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                    Console.ReadKey();
                    return;
                }
            }
            Console.WriteLine("Товар с таким названием не найден");
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void LogIn(string name, string password)
        {
            UserRepository userRepository = new UserRepository();
            foreach (var item in userRepository.GetAll())
            {
                if (item.Name == name)
                {
                    if (item.Password == password)
                    {
                        currentUser = item;
                        Console.WriteLine($"Добро пожаловать {currentUser.Name} !");
                        Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                        Console.ReadKey();
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Неверно введен пароль");
                        Console.ReadKey();
                        return;
                    }

                }
            }
            Console.ReadKey();
            return;
        }

        public static void ViewHistoryOrders()
        {
            OrderRepository orderRepository = new OrderRepository();
            foreach (var item in currentUser.OrdersID)
            {
                if (item != null)
                {
                    var a = orderRepository.Get(item);
                    Console.WriteLine($"Статус заказа :{a.Status}");
                    Console.WriteLine($"Список товаров :");
                    foreach (var prod in a.products)
                        Console.WriteLine($"{prod.Id,-5} |{prod.Name,-20} |{prod.Category,-20} |{prod.Description,-50} |{prod.Price,-5}");
                    Console.WriteLine($"Сумма заказа :{a.Sum}");
                    Console.WriteLine($"\n\n");
                }
            }
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void ViewProduct()
        {
            ProductRepository productRepository = new ProductRepository();
            foreach (var item in productRepository.GetAll())
            {
                Console.WriteLine($"{item.Id,-5} |{item.Name,-20} |{item.Category,-20} |{item.Description,-50} |{item.Price,-5}");
            }
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void ChangeUserData(string name, string password)
        {
            currentUser.Name = name;
            currentUser.Password = password;
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void ChangeProduct(int id, string name, string category, string description, double price)
        {
            ProductRepository productRepository = new ProductRepository();
            if (id == -1)
            {
                Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                Console.ReadKey();
                return;
            }
            foreach (var item in productRepository.GetAll())
            {
                if (item.Id == id)
                {
                    item.Name = name;
                    item.Category = category;
                    item.Description = description;
                    item.Price = price;
                    Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
                    Console.ReadKey();
                    return;
                }
            }
        }

        public static void ChangeDataUsers()
        {
            UserRepository userRepository = new UserRepository();
            Console.WriteLine("Введите номер пользователя");
            int number = Convert.ToInt32(Console.ReadLine()) - 1;
            var user = userRepository.Get(number);
            Console.WriteLine("Введите новое имя пользователя");
            user.Name = Console.ReadLine();
            Console.WriteLine("Введите новый пароль пользователя");
            user.Password = Console.ReadLine();
            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void ChangeStatusOrderForAdmin(int number)
        {
            OrderRepository orderRepository = new OrderRepository();
            var v = orderRepository.Get(number);
            int command = Convert.ToInt32(Console.ReadLine());
            if (command == 1)
                v.Status = "Отмена заказа администратором";
            if (command == 2)
                v.Status = "Получена оплата";
            if (command == 3)
                v.Status = "Отправлен";
            if (command == 4)
                v.Status = "Получено";
            if (command == 5)
                v.Status = "Завершено";

            Console.WriteLine("Нажмите любую клавишу чтобы вернуться в меню");
            Console.ReadKey();
            return;
        }

        public static void Save()
        {
            UserRepository userRepository = new UserRepository();
            userRepository.Update(currentUser.Name, currentUser.Password, currentUser.status, currentUser.Id);
        }
    }
}
