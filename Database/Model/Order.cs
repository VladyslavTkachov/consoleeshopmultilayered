﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Database
{
    public class Order
    {
        double sum;

        public List<Product> products = new List<Product>();
        public string Status
        {
            get;
            set;
        }

        public int Id { get; set; }
        public int IdUser { get; set; }

        public double Sum
        {
            get
            {
                sum = 0;
                foreach (var item in products)
                    sum += item.Price;
                return sum;
            }
        }

    }
}
