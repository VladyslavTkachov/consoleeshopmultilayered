﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database
{
    public class Product
    {
        public int Id
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public double Price
        {
            get;
            set;
        }

        public string Category
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public Product(int id, string name, string category, string description, double price)
        {
            Id = id;
            Name = name;
            Price = price;
            Category = category;
            Description = description;
        }
    }
}
