﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Database
{
    public class User
    {
        public List<int> OrdersID = new List<int>();


        public User(string name, string password)
        {
            Name = name;
            Password = password;
        }
        public User()
        {
            Name = "";
            Password = "";
            status = Status.Guest;
        }

        public User(string name, string password, Status stat, int id)
        {
            Name = name;
            Password = password;
            status = stat;
            Id = id;
        }

        public Status status;
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

      
    }
}
