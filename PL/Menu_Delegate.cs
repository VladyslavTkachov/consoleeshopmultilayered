﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL;

namespace PL
{

    public static class Switch
    {
        public delegate void del();
        public static Dictionary<int, Delegate> GuestDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.RegisterController)},
            {4,new del(Controller.LogInControler)},
            {5,new del(Controller.EndController)}
        };

        public static Dictionary<int, Delegate> UserDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.CreateNewOrderControler)},
            {4,new del(Controller.RegisterOrderControler)},
            {5,new del(Controller.HistoryOrderControler)},
            {6,new del(Controller.StatusForOrderControler)},
            {7,new del(Controller.ChangeDataForUserControler)},
            {8,new del(Controller.ExitControler)}
        };

        public static Dictionary<int, Delegate> AdminDictionary = new Dictionary<int, Delegate>
        {
            {1,new del(Controller.ViewProductControler)},
            {2,new del(Controller.SearchProductControler)},
            {3,new del(Controller.CreateNewOrderControler)},
            {4,new del(Controller.RegisterOrderControler)},
            {5,new del(Controller.ChangeDataForAdminControler)},
            {6,new del(Controller.AddProductControler)},
            {7,new del(Controller.ChangeIformForProductControler)},
            {8,new del(Controller.ChangeStatusOrderForAdminControler)},
            {9,new del(Controller.ExitControler)}
        };
    }
}
