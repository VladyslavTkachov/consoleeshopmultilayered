﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Services;
using BLL.DTO;

namespace PL
{
    public static class Controller
    {
        
        public static void EndController()
        {
            User_Intertface.EndInterface();
        }
        public static void ChoiceInterface()
        {
            if (Performers.currentUser.status == Database.Status.Admin)
                User_Intertface.AdminInterface();
            else if (Performers.currentUser.status == Database.Status.User)
                User_Intertface.UserInterface();
            else
                User_Intertface.GuestInterface();
        }

        public static void Start()
        {
            User_Intertface.Hello();
            ChoiceInterface();
        }

        public static void ViewProductControler()
        {
            User_Intertface.ViewProductInterface();
            Performers.ViewProduct();
            ChoiceInterface();
        }
        public static void SearchProductControler()
        {
            Performers.SearchProduct(User_Intertface.SearchProductInterface());
            ChoiceInterface();
        }
        public static void RegisterController()
        {
            User_Intertface.RegisterInterface(out string name, out string password);
            Performers.CreateNewUser(name, password);
            ChoiceInterface();
        }

        public static void LogInControler()
        {
            User_Intertface.LogInInterface(out string name, out string password);
            Performers.LogIn(name, password);
            ChoiceInterface();
        }

        public static void CreateNewOrderControler()
        {
            User_Intertface.CreateOrderInterface(out List<int> id, out int ID);
            Performers.AddProductToOrder(id, ID);
            Performers.AddOrderToUser(ID);
        }

        public static void RegisterOrderControler()
        {
            User_Intertface.StatusForOrderInterface();
        }

        public static void HistoryOrderControler()
        {
            User_Intertface.ViewHistoryOrderInterface();
            Performers.ViewHistoryOrders();
            ChoiceInterface();
        }

        public static void StatusForOrderControler()
        {
            User_Intertface.StatusForOrderInterface();
        }

        public static void ChangeDataForUserControler()
        {
            User_Intertface.ChangeDataUserInterface(out string name, out string password);
            Performers.ChangeUserData(name, password);
            ChoiceInterface();
        }

        public static void ChangeDataForAdminControler()
        {
            User_Intertface.ViewUsersInterface();
            Performers.ChangeDataUsers();
            ChoiceInterface();
        }

        public static void ExitControler()
        {
            Performers.Save();
            Performers.currentUser = null;
            ChoiceInterface();
        }

        public static void AddProductControler()
        {
            User_Intertface.AddProductToBaseInterface(out int id, out string name, out string category, out string description, out double price);
            Performers.AddProductToBase(id, name, category, description, price);
            ChoiceInterface();
        }

        public static void ChangeIformForProductControler()
        {
            User_Intertface.ChangeInformForItemInterface(out int id, out string name, out string category, out string description, out double price);
            Performers.ChangeProduct(id, name, category, description, price);
            ChoiceInterface();
        }

        public static void ChangeStatusOrderForAdminControler()
        {
            User_Intertface.ChangeStatusOrderForAdminInterface();
            ChoiceInterface();
        }

    }
}
