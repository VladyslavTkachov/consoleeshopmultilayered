﻿using System;
using System.Collections.Generic;
using System.Text;
using BLL.Services;

namespace PL
{
    static class User_Intertface
    {
        public static void EndInterface()
        {

            Performers.Save();
            System.Environment.Exit(0);
        }
        public static void Hello()
        {
            Console.WriteLine($"{" ",40}{"Здраствуйте",20}");
            Console.WriteLine("\n\n\n");
        }

        public static void GuestInterface()
        {
            Console.Clear();
            Console.WriteLine($"Список доступных функций:\n");
            Console.WriteLine($"1 - Просмотр товаров\n");
            Console.WriteLine($"2 - Поиск товара по названию\n");
            Console.WriteLine($"3 - Регистрация нового пользователя\n");
            Console.WriteLine($"4 - Вход в существующую учетную запись\n");
            Console.WriteLine($"5 - Выход из интернет магазина\n");
            Console.WriteLine($"Выберете необходимый пункт меню: ");
            int PunktMenu = Convert.ToInt32(Console.ReadLine());
            Switch.GuestDictionary[PunktMenu].DynamicInvoke();
        }

        public static void UserInterface()
        {
            Console.Clear();
            Console.WriteLine($"Список доступных функций:\n");
            Console.WriteLine($"1 - Просмотр товаров\n");
            Console.WriteLine($"2 - Поиск товара по названию\n");
            Console.WriteLine($"3 - Создание заказа\n");
            Console.WriteLine($"4 - Оформление или отмена заказа\n");
            Console.WriteLine($"5 - Просмотр истории заказов и статуса их доставки\n");
            Console.WriteLine($"6 - Установка статуса заказа Получено \n");
            Console.WriteLine($"7 - Смена персональной информации\n");
            Console.WriteLine($"8 - Выход из учетной записи\n");
            Console.WriteLine($"Выберете необходимый пункт меню: ");
            int PunktMenu = Convert.ToInt32(Console.ReadLine());
            Switch.UserDictionary[PunktMenu].DynamicInvoke();
        }

        public static void AdminInterface()
        {
            Console.Clear();
            Console.WriteLine($"Список доступных функций:\n");
            Console.WriteLine($"1 - Просмотр товаров\n");
            Console.WriteLine($"2 - Поиск товара по названию\n");
            Console.WriteLine($"3 - Создание заказа\n");
            Console.WriteLine($"4 - Оформление заказа\n");
            Console.WriteLine($"5 - Просмотр и изменение персональной информации пользователей\n");
            Console.WriteLine($"6 - Добавление нового товара \n");
            Console.WriteLine($"7 - Смена информации про товар\n");
            Console.WriteLine($"8 - Смена статуса заказа\n");
            Console.WriteLine($"9 - Выход из учетной записи\n");
            Console.WriteLine($"Выберете необходимый пункт меню: ");
            int PunktMenu = Convert.ToInt32(Console.ReadLine());
            Switch.AdminDictionary[PunktMenu].DynamicInvoke();
        }

        public static void ViewProductInterface()
        {
            Console.Clear();
            Console.WriteLine($"{"Id",-5} |{"Название",-20} |{"Категория",-20} |{"Описание",-50} |{"Цена",-5}");
        }
        public static string SearchProductInterface()
        {
            Console.Clear();
            Console.Write($"\n\nВведите название товара: ");
            string name = Console.ReadLine();
            return name;
        }

        public static void RegisterInterface(out string name, out string password)
        {
            Console.Clear();
            Console.Write($"\nВведите свое имя: ");
            name = Console.ReadLine();
            Console.Write($"\n\nВведите пароль: ");
            password = Console.ReadLine();
        }

        public static void LogInInterface(out string name, out string password)
        {
            Console.Clear();
            Console.Write($"\n\nВведите свое имя: ");
            name = Console.ReadLine();
            Console.Write($"\n\nВведите пароль: ");
            password = Console.ReadLine();
        }


        public static void ViewHistoryOrderInterface()
        {
            Console.Clear();
            Console.WriteLine("История заказов:");
        }

        public static void AddProductToBaseInterface(out int id, out string name, out string category, out string description, out double price)
        {
            Console.Clear();
            Console.Write($"\nВведите id товара: ");
            id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in Performers.GetAllProduct())
            {
                if (item.Id == id)
                {
                    Console.Write($"\nТовар с таким id уже существует: ");
                    Console.ReadKey();
                    Controller.ChoiceInterface();
                }
            }
            Console.Write($"Введите название товара: ");
            name = Console.ReadLine();
            Console.Write($"Введите категорию товара: ");
            category = Console.ReadLine();
            Console.Write($"Введите характеристику товара: ");
            description = Console.ReadLine();
            Console.Write($"Введите цену товара: ");
            price = Convert.ToDouble(Console.ReadLine());
        }

        public static void ChangeInformForItemInterface(out int id, out string name, out string category, out string description, out double price)
        {
            Console.Clear();
            Console.Write($"\nВведите id товара: ");
            id = Convert.ToInt32(Console.ReadLine());
            foreach (var item in Performers.GetAllProduct())
            {
                if (item.Id == id)
                {
                    Console.Write($"Введите название товара: ");
                    name = Console.ReadLine();
                    Console.Write($"Введите категорию товара: ");
                    category = Console.ReadLine();
                    Console.Write($"Введите характеристику товара: ");
                    description = Console.ReadLine();
                    Console.Write($"Введите цену товара: ");
                    price = Convert.ToDouble(Console.ReadLine());
                    return;
                }
            }
            id = -1;
            name = null;
            category = null;
            description = null;
            price = -1;
        }

        public static void StatusForOrderInterface()
        {
            Console.Clear();
            Console.WriteLine("Выбрать заказ для изменения статуса");
            foreach (var item in Performers.currentUser.OrdersID)
            {
                foreach (var item2 in Performers.GetAllOrder())
                {
                    if (item == item2.Id)
                    {
                        Console.WriteLine($"Id заказа :{item2.Id}");
                        Console.WriteLine($"Статус заказа :{item2.Status}");
                        Console.WriteLine($"Список товаров :");
                        foreach (var prod in item2.products)
                            Console.WriteLine($"{prod.Id} |{prod.Name} |{prod.Category} |{prod.Description} |{prod.Price}");
                        Console.WriteLine($"Сумма заказа :{item2.Sum}");
                        Console.WriteLine($"\n\n");
                    }
                }
            }
            Console.WriteLine("Выберете заказ: ");
            int number = Convert.ToInt32(Console.ReadLine()) - 1;

            Console.WriteLine("1 - выставить статус Получено ");
            Console.WriteLine("2 - выставить статус Отменено ");
            int n = Convert.ToInt32(Console.ReadLine());
            foreach (var item in Performers.GetAllOrder())
                if (number == item.Id)
                {
                    var a = item;
                    if (n == 1)
                        a.Status = "Получено";
                    if (n == 2 && a.Status != "Получено")
                        a.Status = "Отменен";
                    break;
                }

           
            Console.ReadKey();
            Controller.ChoiceInterface();
        }

        public static void ChangeDataUserInterface(out string name, out string password)
        {
            Console.Write($"Введите новое имя: ");
            name = Console.ReadLine();
            Console.Write($"Введите новый пароль: ");
            password = Console.ReadLine();
        }

        public static void CreateOrderInterface(out List<int> id, out int ID)
        {
            Console.WriteLine($"Выберете товар по id (через пробел или запятую) ");
            id = new List<int>();
            string str = Console.ReadLine();
            string s = "";
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != ' ' && str[i] != ',' && str[i] != '\0')
                    s += str[i];
                else
                {
                    id.Add(Convert.ToInt32(s));
                    s = "";
                }
            }
            id.Add(Convert.ToInt32(s));
            Random rnd = new Random();
            ID = rnd.Next(0,100);
        }

        public static void ChangeStatusOrderForAdminInterface()
        {
            foreach (var user in Performers.GetAllUser())
            {
                Console.WriteLine($"Имя закажчика :{user.Name}");
                foreach (var item in Performers.GetAllOrder())
                {
                    Console.WriteLine($"Id заказа :{item.Id}");
                    Console.WriteLine($"Id пользователя :{item.IdUser}");
                    Console.WriteLine($"Статус заказа :{item.Status}");
                    Console.WriteLine($"Список товаров :");
                    foreach (var prod in item.products)
                        Console.WriteLine($"{prod.Id,-5} |{prod.Name,-20} |{prod.Category,-20} |{prod.Description,-50} |{prod.Price,-5}");
                    Console.WriteLine($"Сумма заказа :{item.Sum}");
                    Console.WriteLine($"\n\n");
                }
            }

            Console.WriteLine("Введите номер заказа");
            int number = Convert.ToInt32(Console.ReadLine()) - 1;
            Console.WriteLine("Выберете статус:");
            Console.WriteLine("1 - Отмена заказа администратором");
            Console.WriteLine("2 - Получена оплата");
            Console.WriteLine("3 - Отправлено");
            Console.WriteLine("4 - Получено");
            Console.WriteLine("5 - Завершено");
            Performers.ChangeStatusOrderForAdmin(number);
        }
        public static void ViewUsersInterface()
        {
            foreach (var user in Performers.GetAllUser())
            {
                Console.WriteLine($"Id: {user.Id,-5} Name: {user.Name,-20}  Password: {user.Password,-20}");
            }
        }
    }
}
