﻿using System;
using System.Collections.Generic;
using System.Text;
using Database;

namespace DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<User> Users { get; }
        IRepository<Order> Orders { get; }
        IRepository<Product> Products { get; }
        void Save();
    }
}
