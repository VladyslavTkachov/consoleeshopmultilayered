﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using Database;
using System.Linq;

namespace DAL.Repositories
{
    public class ProductRepository: IRepository<Product>
    {
        public IEnumerable<Product> GetAll()
        {
            return Products.Goods.ToArray(); 
        }

        public Product Get(int id)
        {
            return Products.Goods.Find(i => i.Id == id);
        }

        public void Create(Product product)
        {
            Products.Goods.Add(product);
        }

        public void Update(Product product)
        {
            var a = Products.Goods.Find(i => i.Id == product.Id);
            a = product;
        }

        public IEnumerable<Product> Find(Func<Product, Boolean> predicate)
        {
            return Products.Goods.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            Product product = Products.Goods.Find(i => i.Id == id);
            if (product != null)
                Products.Goods.Remove(product);
        }
    }
}
