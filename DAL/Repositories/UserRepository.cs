﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL.Interfaces;
using Database;

namespace DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        public IEnumerable<User> GetAll()
        {
            return Users.User.ToArray();
        }

        public User Get(int id)
        {
            return Users.User.Find(i => i.Id == id);
        }

        public void Create(User user)
        {
           Users.User.Add(user);
        }

        public void Update(string name, string password, Status stat, int id)
        {
            User user = new User(name, password, stat, id);
            var a = Users.User.Find(i => i.Id == user.Id);
            a = user;
        }
        public void Update(User user)
        {
            
        }

        public IEnumerable<User> Find(Func<User, Boolean> predicate)
        {
            return Users.User.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            User user = Users.User.Find(i => i.Id == id);
            if (user != null)
                Users.User.Remove(user);
        }
    }
}
