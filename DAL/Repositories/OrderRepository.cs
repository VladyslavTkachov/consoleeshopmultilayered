﻿using System;
using System.Collections.Generic;
using System.Text;
using Database;
using DAL.Interfaces;
using System.Linq;

namespace DAL.Repositories
{
    public class OrderRepository: IRepository<Order>
    {
        public IEnumerable<Order> GetAll()
        {
            return Orders.orders.ToArray();
        }

        public Order Get(int id)
        {
            return Orders.orders.Find(i => i.Id == id);
        }

        public void Create(Order order)
        {
            Orders.orders.Add(order);
        }

        public void Update(Order order)
        {
            var a = Orders.orders.Find(i => i.Id == order.Id);
            a = order;
        }

        public IEnumerable<Order> Find(Func<Order, Boolean> predicate)
        {
            return Orders.orders.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            Order order = Orders.orders.Find(i => i.Id == id);
            if (order != null)
                Orders.orders.Remove(order);
        }
    }
}
