﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Entities;

namespace DAL.DB
{
    public static class Users
    {
        public static List<User> User = new List<User>() { new User("admin", "admin", Status.Admin) };
        public static User CurrentUser = new User();
    }
}
